﻿using UnityEngine;

public class LinkBehavior : MonoBehaviour
{

    internal NodeBehavior nodeA;
    internal NodeBehavior nodeB;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.gameIsOver)
        {

        }
    }

    private void OnMouseDown()
    {
        if (!GameManager.instance.gameIsOver)
        {
            GameManager.instance.Remove(this);
            nodeA = null;
            nodeB = null;
            Destroy(this.gameObject);
        }
    }

    internal static LinkBehavior Create(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        // Create the cylinder
        GameObject link = Instantiate(GameManager.instance.linkPrefab) as GameObject;
        link.transform.parent = nodeA.transform.parent;

        LinkBehavior linkBehavior = link.GetComponent<LinkBehavior>();
        linkBehavior.nodeA = nodeA;
        linkBehavior.nodeB = nodeB;

        // Place it between the 2 nodes
        Vector3 v3a = nodeA.gameObject.transform.position;
        Vector3 v3b = nodeB.gameObject.transform.position;
        link.transform.position = (v3b - v3a) / 2.0f + v3a;

        // Scale height to distance between the 2 nodes
        Vector3 v3T = link.transform.localScale;
        v3T.y = (v3b - v3a).magnitude / 2;
        link.transform.localScale = v3T;

        // Rotate it to point from one to the other node
        link.transform.rotation = Quaternion.FromToRotation(Vector3.up, v3b - v3a);

        return linkBehavior;
    }

}

