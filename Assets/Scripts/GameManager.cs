﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{


    // Make game manager public static so can access this from other scripts
    public static GameManager instance;

    // Gameobject for a Link
    public GameObject linkPrefab;

    // UI Text to show time
    public Text clockDisplay;

    // UI Text to show level
    public Text levelDisplay;

    // Restart button
    public Button restartButton;

    // NExt level button
    public Button nextLevelButton;

    public GameObject outline;

    // Reference to AudioClip to play
    public AudioClip waveSFX;

    // Reference to AudioClip to play
    public AudioClip gameOverSFX;

    // Camera with audio listener
    public Camera mainCamera;

    // Initial time 
    public int initialClockTicks = 30;

    public int ticksPerWave = 3;

    public float secondsBetweenTicks = 1.0f;

    public float revealDuration = 0.5f;

    public int thisLevel;

    internal bool gameIsOver = false;

    internal List<NodeBehavior> nodes;

    private List<LinkBehavior> links;

    private NodeBehavior selectedNode;

    private bool waving;

    private int clockTicksLeft;

    private float nextClockTick;


    // Use this for initialization
    void Start()
    {
        // Initialize lists 
        links = new List<LinkBehavior>();
        nodes = new List<NodeBehavior>();

        // get a reference to the GameManager component for use by other scripts
        if (instance == null)
            instance = this.gameObject.GetComponent<GameManager>();

        clockTicksLeft = initialClockTicks;

        clockDisplay.text = "" + clockTicksLeft;
        levelDisplay.text = "Level " + thisLevel;

        nextClockTick = Time.time + secondsBetweenTicks;

    }

    // Update is called once per frame
    void Update()
    {
        if (!gameIsOver)
        {

            if (Time.time >= nextClockTick)
            {
                clockTicksLeft = clockTicksLeft - 1;

                clockDisplay.text = "" + clockTicksLeft;

                nextClockTick = Time.time + secondsBetweenTicks;

                if (clockTicksLeft < 1)
                {
                    EndGame();
                }
                else if (clockTicksLeft != initialClockTicks)
                {
                    // Wave start trigger (every 3 ticks)
                    if (clockTicksLeft % ticksPerWave == 0)
                    {
                        clockDisplay.color = Color.red;

                        // If not in a wave, start one
                        if (!waving)
                        {
                            StartWave();
                        }
                    } else
                    {
                        clockDisplay.color = Color.white;
                    }


                }

            }
        }

    }

    public void OnSelectionChange(NodeBehavior node) 
    {

        if (node.selected)
        {
            ShowSelectionBox(node);

            if (selectedNode == null)
            {
                selectedNode = node;
            }
            else
            {
                selectedNode.LinkTo(node);
                selectedNode = null;
            }
        }
        else
        {
            StartCoroutine(HideSelectionBox());
            if (selectedNode == node)
            {
                selectedNode = null;
            }
        }

    }

    private void ShowSelectionBox(NodeBehavior node)
    {
        Vector3 nodeCenterInTheWorld = node.transform.position;
        Vector3 nodeRightEdgeInTheWorld = new Vector3(nodeCenterInTheWorld.x + node.GetSize() / 2.0f, nodeCenterInTheWorld.y, nodeCenterInTheWorld.z);
        float nodeCenterXScreenPosition = mainCamera.WorldToScreenPoint(nodeCenterInTheWorld).x;
        float nodeRightEdgeXScreenPosition = mainCamera.WorldToScreenPoint(nodeRightEdgeInTheWorld).x;
        float screenSize = (nodeRightEdgeXScreenPosition - nodeCenterXScreenPosition) * 2;


        // Show selection box
        outline.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, screenSize * 1.2f);
        outline.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, screenSize * 1.2f);
        outline.transform.position = mainCamera.WorldToScreenPoint(node.transform.position);
        outline.SetActive(true);
    }

    IEnumerator HideSelectionBox()
    {
        yield return new WaitForSeconds(0.5f);
        outline.SetActive(false);
    }



    public void Add(LinkBehavior link)
    {
        links.Add(link);
        DefineTickSpeed();
    }

    public void Remove(LinkBehavior link)
    {
        links.Remove(link);
        DefineTickSpeed();
    }

    public void NextLevel()
    {

        int nextLevel = thisLevel + 1;
        if (nextLevel > SceneManager.sceneCountInBuildSettings)
        {
            nextLevel = 1;
        }
        SceneManager.LoadScene("Level" + nextLevel);
    }


    public void Restart()
    {
        string levelToLoad = "Level" + thisLevel;
        SceneManager.LoadScene(levelToLoad);
    }

    private void StartWave()
    {
        //Debug.Log("Starting wave!");

        waving = true;

        AudioSource.PlayClipAtPoint(waveSFX, mainCamera.transform.position);

        RevealNodes();

        StartCoroutine(EndWave());
    }

    private void RevealNodes()
    {
        foreach (var node in nodes)
        {
            node.Reveal();

        }
    }

    IEnumerator EndWave()
    {
        //Debug.Log("Computing...");

        ComputeInteractions();

        CheckLinks();

        //Debug.Log("Computed.");

        yield return new WaitForSeconds(revealDuration);

        DisguiseNodes();

        waving = false;
    }

    private void DisguiseNodes()
    {
        foreach (var node in nodes)
        {
            node.Disguise();
        }
    }

    private void ComputeInteractions()
    {
        // For each link
        foreach (var link in links)
        {
            // Nodes from the link
            NodeBehavior nodeA = link.nodeA;
            NodeBehavior nodeB = link.nodeB;

            //Debug.Log("Link:" + nodeA.name + "-" + nodeB.name);

            nodeA.InteractWith(nodeB);
            nodeB.InteractWith(nodeA);
        }
    }

    private void CheckLinks()
    {

        List<LinkBehavior> linksToRemove = new List<LinkBehavior>();


        foreach (var link in links)
        {
            // Nodes from the link
            NodeBehavior nodeA = link.nodeA;
            NodeBehavior nodeB = link.nodeB;

            if (nodeA.GetSize() < 1 || nodeB.GetSize() < 1)
            {
                linksToRemove.Add(link);
            }

        }

        foreach (var link in linksToRemove)
        {
            Remove(link);
            Destroy(link.gameObject);

        }

    }

    private void EndGame()
    {
        //Debug.Log("Game is OVER");

        gameIsOver = true;


        RevealNodes();

        AudioSource.PlayClipAtPoint(gameOverSFX, mainCamera.transform.position);

        outline.SetActive(false);


        // Iterate over links, adding linked nodes to a set
        HashSet<NodeBehavior> linkedNodes = new HashSet<NodeBehavior>();
        foreach (var link in links)
        {
            if (link.nodeA.GetSize() > 0)
            {
                linkedNodes.Add(link.nodeA);
            }
            if (link.nodeB.GetSize() > 0)
            {
                linkedNodes.Add(link.nodeB);
            }
        }

        foreach (var node in nodes)
        {
            if (!linkedNodes.Contains(node))
            {
                node.gameObject.AddComponent<Rigidbody>();
            }
        }

        if (linkedNodes.Count < nodes.Count)
        {
            clockDisplay.text = "YOU DROPPED THE BALL";
            clockDisplay.color = Color.red;
            restartButton.gameObject.SetActive(true);
        }
        else
        {
            clockDisplay.text = "WELL DONE!";
            clockDisplay.color = Color.cyan;
            nextLevelButton.gameObject.SetActive(true);
        }


    }

    private void DefineTickSpeed()
    {
        float baseLinks = nodes.Count;

        float minWaveDuration = 0.5f;         
        float maxWaveDuration = ticksPerWave; 

        float minSecondsBetweenTicks = minWaveDuration / ticksPerWave; // 0.33f
        float maxSecondsBetweenTicks = maxWaveDuration / ticksPerWave; // 1f

        float minRevealDuration = minWaveDuration / 5.0f; // 0.2f
        float maxRevealDuration = maxWaveDuration / 6.0f; // 0.5f

        secondsBetweenTicks = Mathf.Lerp(maxSecondsBetweenTicks, minSecondsBetweenTicks, links.Count / baseLinks);
        revealDuration = Mathf.Lerp(maxRevealDuration, minRevealDuration, links.Count / baseLinks);


        //Debug.Log(links.Count + ";" + baseLinks + ";" + secondsBetweenTicks + ";" + revealDuration);
    }

}
