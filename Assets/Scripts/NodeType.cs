﻿using UnityEngine;

public abstract class NodeType
{
    internal Material material;

    public abstract void Interact(NodeBehavior nodeA, NodeBehavior nodeB);

}

public class Inert : NodeType
{
    public override void Interact(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        // Inert means there are no changes.
    }
}

public class Steal : NodeType
{
    int amount = 1;

    public override void Interact(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        //Debug.Log(nodeA + " steals " + amount + " from " + nodeB);

        int subtracted = nodeB.Subtract(amount);
        nodeA.Add(subtracted);
        //Debug.Log("Result:" + nodeA + ", " + nodeB);
    }
}

public class Give : NodeType
{
    int amount = 1;

    public override void Interact(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        //Debug.Log(nodeA + " gives " + amount + " to " + nodeB);

        int subtracted = nodeA.Subtract(amount);
        nodeB.Add(subtracted);
        //Debug.Log("Result:" + nodeA + ", " + nodeB);
    }
}

public class Create : NodeType
{
    int amount = 1;

    public override void Interact(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        //Debug.Log(nodeA + " adds " + amount + " to " + nodeB);

        nodeB.Add(amount);
        //Debug.Log("Result:" + nodeA + ", " + nodeB);
    }
}

public class Destroy : NodeType
{
    int amount = 1;

    public override void Interact(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        //Debug.Log(nodeA + " subtracts " + amount + " from " + nodeB);

        nodeB.Subtract(amount);
        //Debug.Log("Result:" + nodeA + ", " + nodeB);
    }
}

public class Double : NodeType
{
    public override void Interact(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        //Debug.Log(nodeA + " doubles " + nodeB);

        nodeB.Add(nodeB.GetSize());
        //Debug.Log("Result:" + nodeA + ", " + nodeB);
    }
}

public class Divide : NodeType
{
    public override void Interact(NodeBehavior nodeA, NodeBehavior nodeB)
    {
        //Debug.Log(nodeA + " divides by 2 " + nodeB);

        nodeB.Subtract(nodeB.GetSize() / 2);
        //Debug.Log("Result:" + nodeA + ", " + nodeB);
    }
}
