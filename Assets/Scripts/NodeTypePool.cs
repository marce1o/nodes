﻿
using UnityEngine;



public class NodeTypePool
{

    private NodeType[] types;

    public NodeTypePool(Material[] nodeTypesMaterials)
    {

        NodeType[] availableTypes = { 
            new Inert(), new Steal(), new Give(), new Create(), new Destroy(), new Double(), new Divide()
        };

        types = new NodeType[nodeTypesMaterials.Length];

        for (int i = 0; i < nodeTypesMaterials.Length; i++)
        {
            types[i] = availableTypes[i];
            types[i].material = nodeTypesMaterials[i];
        }
    }

    internal NodeType Randomize()
    {
        int index = Random.Range(0, types.Length);
        return types[index];
    }
}
