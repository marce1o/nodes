﻿using UnityEngine;

public class LevelSpawner : MonoBehaviour
{
    // public variables

    public float secondsBetweenSpawning = 0.1f;

    public float xMinimumVariation = -5;
    public float xMaximumVariation = 5;

    public float yMinimumVariation = -5;
    public float yMaximumVariation = 5;

    public float zMinimumVariation = -5;
    public float zMaximumVariation = 5;

    public int minimumSize = 1;

    public int maximumSize = 5;

    public Material[] nodeTypesMaterials;

    public GameObject[] nodes;

    // private variables

    private float nextSpawnTime;

    private int nodesCounter = 0;

    private NodeTypePool nodeTypePool;

    // Use this for initialization
    void Start()
    {
        // determine when to spawn the next object
        nextSpawnTime = Time.time + secondsBetweenSpawning;

        nodeTypePool = new NodeTypePool(nodeTypesMaterials);
    }

    // Update is called once per frame
    void Update()
    {
        // if time to spawn a new game object
        if (Time.time >= nextSpawnTime)
        {
            // Spawn the game object through function below
            MakeThingToSpawn();

            // determine the next time to spawn the object
            nextSpawnTime = Time.time + secondsBetweenSpawning;
        }
    }

    void MakeThingToSpawn()
    {

        if (nodesCounter < nodes.Length)
        {
            GameObject node = nodes[nodesCounter];

            // Apply a random variation to Node position

            Vector3 spawnPosition = node.transform.position;

            spawnPosition.x += Random.Range(xMinimumVariation, xMaximumVariation);
            spawnPosition.y += Random.Range(yMinimumVariation, yMaximumVariation);
            spawnPosition.z += Random.Range(zMinimumVariation, zMaximumVariation);

            node.transform.position = spawnPosition;

            NodeBehavior nodeBehavior = node.GetComponent<NodeBehavior>();

            // Define a random type for the Node
            nodeBehavior.SetNodeType(nodeTypePool.Randomize());

            // Define a random size for the Node
            nodeBehavior.Add(Random.Range(minimumSize - 1, maximumSize + 1));

            // Add to Game Manager
            GameManager.instance.nodes.Add(nodeBehavior);
            //Debug.Log("NEW Node: " + nodeBehavior);

            // Display the Node
            node.SetActive(true);

            nodesCounter++;

            // Make the Node's parent the spawner so hierarchy doesn't get super messy
            node.transform.parent = gameObject.transform;

        }

    }
}
