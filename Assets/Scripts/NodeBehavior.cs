﻿using System;
using System.Collections;
using UnityEngine;

public class NodeBehavior : MonoBehaviour
{

    public float alphaForZero = 0.3f;

    public float resizingDuration = 0.5f;

    internal bool selected = false;

    private int size;

    private NodeType nodeType;

    private Renderer nodeRenderer;

    private Material disguiseMaterial;

    private Material typeMaterial;

    private readonly Vector3 VECTOR_1 = new Vector3(1, 1, 1);


    // Use this for initialization
    void Start()
    {
        nodeRenderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.gameIsOver)
        {

        }
    }

    private void OnDestroy()
    {
        Destroy(disguiseMaterial);
        Destroy(typeMaterial);
    }

    // Called when MouseDown on this gameObject
    void OnMouseDown()
    {
        if (!GameManager.instance.gameIsOver)
        {
            ToggleSelection();
        }
    }

    internal string GetName()
    {
        return gameObject.name;
    }

    // Select the node
    internal void ToggleSelection()
    {
        selected = !selected;
        GameManager.instance.OnSelectionChange(this);
    }

    internal NodeType GetNodeType()
    {
        return nodeType;
    }

    internal void SetNodeType(NodeType newNodeType)
    {
        nodeType = newNodeType;
        typeMaterial = Instantiate(nodeType.material);
        disguiseMaterial = Instantiate(nodeRenderer.material);
    }

    internal int GetSize()
    {
        return size;
    }

    internal int Add(int amount)
    {
        // Increase size
        size += amount;

        OnSizeChanged();

        return amount;
    }

    internal int Subtract(int amount)
    {
        // What is the actual amount that I can subtract?
        int actual = Math.Min(size, amount);

        // Decrease size
        size -= actual;

        OnSizeChanged();

        return actual;
    }

    internal void LinkTo(NodeBehavior node)
    {
        LinkBehavior link = LinkBehavior.Create(this, node);
        GameManager.instance.Add(link);

        // De-select nodes
        ToggleSelection();
        node.ToggleSelection();
    }

    internal void InteractWith(NodeBehavior other)
    {
        nodeType.Interact(this, other);
    }

    internal void Reveal()
    {
        nodeRenderer.material = typeMaterial;
    }

    internal void Disguise()
    {
        nodeRenderer.material = disguiseMaterial;
    }

    private void ChangeAlphas(float newAlpha)
    {
        Color color1 = disguiseMaterial.color;
        color1.a = newAlpha;
        disguiseMaterial.color = color1;

        Color color2 = typeMaterial.color;
        color2.a = newAlpha;
        typeMaterial.color = color2;
    }

    private void OnSizeChanged()
    {
        if (size > 0)
        {
            //gameObject.transform.localScale = new Vector3(size, size, size);
            StartCoroutine(Resize());


            ChangeAlphas(1f);
        }
        else
        {
            gameObject.transform.localScale = VECTOR_1;
            ChangeAlphas(alphaForZero);
        }
    }

    IEnumerator Resize()
    {
        Vector3 actualScale = transform.localScale;
        Vector3 targetScale = new Vector3(size, size, size);

        for (float t = 0; t < 1; t += Time.deltaTime / resizingDuration)
        {
            transform.localScale = Vector3.Lerp(actualScale, targetScale, t);
            yield return null;
        }
    }

    public override string ToString()
    {
        string header = base.ToString();
        return header + "[name:" + name + ",size:" + size + ",type:" + nodeType + "]";
    }

}
